#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "myvect.h"
#include "quee.h"


using namespace std;


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->listView->setVisible(false);

    myVect testVect;
    Quee queeVect;

    testVect.push_back(5); //0
    testVect.push_back(4); //1
    testVect.push_back(3); //2

    testVect[1]=2;

    testVect[0] = 2; //teraz zwraca error, a powinno być to prawidłowe działanie

    cout << testVect[0] << endl;
    cout << testVect[1] << endl;
    cout << testVect[2] << endl;
    cout << testVect[33] << endl;

    queeVect.push_back(1);
    queeVect.push_back(2);
    queeVect.push_back(43);
    queeVect.push_back(3);
    queeVect.push_back(12);

    queeVect.popBack();

    queeVect.printall();


}

MainWindow::~MainWindow()
{
    delete ui;
}


 void MainWindow::on_dodajButton_clicked()
{

    string full_name = ui->textEdit->toPlainText().toStdString();
    string name, surname;

    int pos = 0;
    int spaces = 0;

    for (auto it : full_name)
    {
        if (it == ' ')
        {
            spaces++;
        }
    }
    if (spaces != 1)
    {
    cout << "bledny format" << endl;
    return;

    }
    for (auto it : full_name)
    {
        if (it == ' ')
        {
            name.assign(full_name.begin(), full_name.begin() + pos);
            break;
        }
        pos++;
    }
    surname.assign(full_name.begin() + pos + 1, full_name.end());
    names.push_back(make_pair (name,surname));

    cout<<surname<<name<<endl;
}


void MainWindow::on_sprawdzButton_clicked()
{
    ui->listView->setVisible(true);
}




void MainWindow::on_pokazwszystkieButton_clicked(bool checked)
{

}

/**********************zadane**********
1. W tym momeenice działa odczyt z naszego wektora czyli

cout << testVect[2] << endl;

Wyświetli nam liczbę która zanjduje się na trzeciej pozycji. Jednak niemożliwe jest wykonanie operacji
przypisania:

testVect[2] = 2;

Dlaczego? Jak można zrobić żeby działała.

2. Odwołując się do elemntu wektora poza zakresem nie ma obsługi błędu, czyli jeśli wektor ma trzy
elementy a my odwołamy się do testVect[100], powinien być zgłoszony błąd.

3. Poczytaj o szablonach i spróbuj przerobić nasz wektor na typ szablonowy */
/********************************/
/*
1. Zadanie 1 rozwiązałeś dobrze jednak teraz chciałbym żeby nie było dodatkowej funkcji tylko żeby samo działanie

testVect[2] = 2

Powodowało napisanie wartość. Podpowiedź - "referencja"

2. Zabawa z gitem. Stwórz nową branche (powiedzmy "Quee_feature") na podstawie "master". Następne zadanie zrób
na nowej branchy i jak skończysz do zrób merge do "master"

3. No to trochę obiekotowo polecimy. Stwórz nową klasę "Quee.cpp"
    - musi dziedziczyć po myVect i mieć o jedną metodę więcej - popBack(), która zwraca ostatni element
    - (opcjonalnie) zrób tak, żeby operator[], był niedostępny (private) poza klasą Quee, jednak w myVect() pozostała public
    - w MyVect metodę printAll() zrób virtualną. Zrób tak żeby printAll() w Quee wyświetlało elementy w odwrotnej kolejności. */
