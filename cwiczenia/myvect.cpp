#include "myvect.h"
#include <iostream>




myVect::myVect()
{
    container = NULL;
    size = 0;
    position = 0;
    real_size = 0;

    moje = 0;
}

myVect::myVect(int initial_size)
{
    container = new int[initial_size + MEMOR_STEP];
    size = initial_size;
    position = initial_size;
    real_size = initial_size + MEMOR_STEP;

}



void myVect::push_back( int value)
{
    if (position == real_size)
    {
        int *tmp_pointer = NULL;
        tmp_pointer = (int*)realloc(container,10 * sizeof (int));
        container = tmp_pointer;
        real_size =+ MEMOR_STEP;
    }

    container[position]= value;
    size = position;
    position++;

}

int & myVect::operator [](int pos)
{
    if (pos>size) //2. Test - czy jest tyle miejsc wektora
    {
        std::cout<<"Nie stworzyles "<<pos<<" miejsc wektora! Ostatnia, najwieksza liczba miejsca, to "<<size<<" przechowujaca wartosc: ";
        return container[size];

    }
    else
    {
        return container[pos];
    }

}

void myVect::printall() //bez polimorficznego wskaznika, bo nie ma na co wskazywac
{
    for (int i = 0; i < size+1; i++)
    {
        std::cout << container[i] << std::endl;
    }
}

void myVect::change(int numerVektoraDoZmiany, int wartoscNaJakaZmienic) // 1. Podmiana przypisania wartosci
{
    position = numerVektoraDoZmiany;
    container[position]=wartoscNaJakaZmienic;
}
