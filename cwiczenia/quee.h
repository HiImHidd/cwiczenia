#ifndef QUEE_H
#define QUEE_H

#include "myVect.h"


class Quee:public myVect
{

public:
    Quee();
    int popBack();
    void printall();

private:
    int & operator [](int pos);

};

#endif // QUEE_H
