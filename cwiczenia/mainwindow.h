#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <vector>

using namespace std;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    vector <pair <string, string>> names;
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:

    void on_dodajButton_clicked();


    void on_sprawdzButton_clicked();

private slots:


    void on_pokazwszystkieButton_clicked(bool checked);

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
