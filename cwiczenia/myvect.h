#ifndef MYVECT_H
#define MYVECT_H
#include <stdlib.h>



#define MEMOR_STEP 8

class myVect
{

public:
    myVect();
    myVect(int initial_size);
    void push_back(int value);
    int & operator [](int pos);
    virtual void printall();
    void change(int numerVektoraDoZmiany, int wartoscNaJakaZmienic);

protected:
    int *container;
    int size;
    int position;
    int real_size;
    int moje;

};

#endif // MYVECT_H
